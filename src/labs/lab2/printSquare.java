package labs.lab2;

import java.util.Scanner;

public class printSquare {

	public static void main(String[] args) {
		// program that will prompt the user for a number and print out a square with those dimension

		Scanner input = new Scanner(System.in); // get user input 
		
		System.out.print("Size: ");
		int size = Integer.parseInt(input.nextLine());
		input.close();
		
		for  (int roll=0; roll<=size; roll++) { // rolls
			for ( int col=0; col<=size; col++) { // columns 
				System.out.print("* ");
			}
			System.out.println();
		}
		
		
	}

}
