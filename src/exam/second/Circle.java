package exam.second;

public class Circle extends Polygon {
	private double radius;
	
	public Circle() {
		this.radius = radius;
	}

	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}

	public String toString() {
		return "Radius: " + radius;
	}
	
	public double area() {
		return Math.PI*radius*radius;
	}
	
	public double perimeter() {
		return 2 * Math.PI * radius;
	}
}
