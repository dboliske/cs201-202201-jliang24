package labs.lab3;

import java.util.Scanner;

public class ExerciseThree {

	public static void main(String[] args) {
		
//		Write a program that will find the minimum value and print it to the console for the given array:
		
		Scanner input = new Scanner (System.in);
		
		//array for the values 
		int [] values = new int[] {72, 101, 108, 108, 111, 32, 101, 118, 101, 114, 121, 111, 110, 101, 33, 32, 76, 111, 111, 107, 32, 97, 116, 32, 116, 104, 101, 115, 101, 32, 99, 111, 111, 108, 32, 115, 121, 109, 98, 111, 108, 115, 58, 32, 63264, 32, 945, 32, 8747, 32, 8899, 32, 62421};
		
		 double min = getMin(values);
		 System.out.println("The minimum values is " + min );
	}
		 
		//calculating for minimum
	
	public static int getMin(int[] inputValues) {
		int minValue = inputValues[0];
		for (int i=0; i<inputValues.length; i++) {
			if (inputValues[i] < minValue) {
				minValue = inputValues[i];
			}
		}
		return minValue;

	}
}
