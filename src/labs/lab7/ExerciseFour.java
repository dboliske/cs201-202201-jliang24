package labs.lab7;

public class ExerciseFour {

	public static void printRev(String [] data) {
		printRev(data, 0 , data.length - 1);
	}
	
	public static void printRev(String [] data, int first, int last) {
		if (first > last) {
			return;
		}
		System.out.print(data[last] + " ");
		printRev(data, first, last -1 );
	}
	
	public static void main(String[] args) {
	
		String [] data = {"c", "html", "java", "python", "ruby", "scala"};
		printRev(data);
		
		 }
	}

