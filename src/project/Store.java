package project;

import java.io.Console;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;


public class Store {
//	Your program must allow a user to create a new items and add them to the stores stock. This should include the ability to add duplicates of the same item.
//	Your program must allow a user to sell items and remove them from the store. This should allow a user to add the item to a cart first before choosing to check out.
//	Your program must allow a user to search for an item in the store with a given name supplied by the user.
//	Your program must allow a user to modify items in the store (e.g. updating the price or name). These changes need to be applied to all instance of the item.
//	Your program must be able to exit when the user requests to finish.
	
	public String item;
	public Object slots;
	public String date;
	
	public Store(){
		item = null;
		slots = null;
		date = null;
	}
	
	public Store(String item, Object slots, String date) {
		this();
		setItem(item);
		setSlots(slots);
		setDate(date);
	}
	

	public String getItem() {
		return item;
	}

	public void setItem(String item) {
		this.item = item;
	}

	public Object getSlots() {
		return slots;
	}

	public void setSlots(Object slots) {
		this.slots = slots;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
	
	public String toString() {
		return date = "04" + "/" + "29" + "/" + "2022";
	} 

	public static void main(String[] args) throws FileNotFoundException {
		Scanner input = new Scanner(System.in);
				
		System.out.println("Options: ");
		System.out.println("1 - Add Items");
		System.out.println("2 - Remove Item");
		System.out.println("3 - Search for Item");
		System.out.println("4 - exit");
		System.out.println();
		
		boolean done = false;
		String option = input.nextLine();
		 do {
			 System.out.println("Enter \"1\", \"2\" , \"3\" or \"4\"");
			 
			 switch(option) {
			 case "1": 
				 //adding items
				 ArrayList<String> strList = new ArrayList<String>();
					strList.add("banana");
					strList.add("carrot");
					strList.add("apple");
					strList.add("avocado");
					strList.add("bread");
					strList.add("bag of chips");
					strList.add("P");
					strList.add("pen");
					strList.add("pencil");
					strList.add("pad of paper");
					strList.add("bottle of juice");
					strList.add("wine");
					strList.add("beer");
					strList.add("cold meds");
					System.out.println("Stock: " + strList);
				 break;
			 case "2":
				 //choice to remove items
				System.out.println("Slock list: " + "removeList");
				//strList.remove(banana); ....
				 break;
			 case "3":
				 //choice to search for items
				 try {
					File f = new File("stock.csv");
					Scanner myReader = new Scanner(f);
					while (myReader.hasNextLine()) {
						String data = myReader.nextLine();
						System.out.print(data);
					}
				myReader.close();
				} catch (Exception e) {
					System.out.println("An error has occur.");
					e.printStackTrace();
				}
			 case "4": 
				 // exit the program when done
				 done = true;
					break;
				default: 
					System.out.println("End of the program!!");
				 
			 }
		 
		 } while(!done);
		 System.out.println("goodbye!!");
		 input.close();
	}
}
