package labs.lab1;

import java.util.Scanner;

public class Temperature {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//test program   testvalues   
		//low          -10 F, -10 C      
		//high          100 F, 90 C 
		//middle         60 F,  15 C
		// My expected results were having the correct calculation for the convertion. Based on converting from Fahrenheit to Celsius or Celsius to Fahrenheit.
		// My result was returning the convertion in decimal using double F and double  C

		Scanner input = new Scanner (System.in);
		
		System.out.print("Enter Fahrenheit: ");
		
		// convert the Fahrenheit to Celsius and display the result.
	
		double F = Double.parseDouble(input.nextLine());
		System.out.println((F - 32) * (5./9.));
		
		
		//prompt the user
		System.out.println("Enter Celsius: ");
		
		//convert the Celsius to Fahrenheit and display the result.
		double  C = Double.parseDouble(input.nextLine());
		System.out.println(C *(9./5.) + 32 );
		
		
		input.close();
	}

}
