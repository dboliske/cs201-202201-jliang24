package exam.second;

public class Classroom {
	private String building;
	protected String roomNumber;
	private int seat;
	
	
	public Classroom() {
		String building = "Robert Healy";
		String roomNumber = "101";
		int seat = 31;			
	}
	
	public Classroom(String building, String roomNumber, int seat) {
		this.building = building;
		this.roomNumber = roomNumber;
		this.seat = seat;
	}

	public String getBuilding() {
		return building;
	}

	public void setBuilding(String building) {
		this.building = building;
	}

	public String getRoomNumber() {
		return roomNumber;
	}

	public void setRoomNumber(String roomNumber) {
		this.roomNumber = roomNumber;
	}

	public int getSeat() {
		return seat;
	}

	public void setSeat(int seat) {
		this.seat = seat;
	}
	
	public String toString() {
		return building + "," + roomNumber + seat;
	}
	
	
}


