package exams.first;

import java.util.Scanner;

public class Selection {

	public static void main(String[] args) {
		//Write a program that prompts the user for an integer. 
		//If the integer is divisible by 2 print out "foo", and if the integer is divisible by 3 print out "bar". 
		//If the integer is divisible by both, your program should print out "foobar" and if the integer is not divisible by either,
		//then your program should not print out anything.
		
		Scanner input = new Scanner(System.in);
		System.out.println("Enter in number:");
		
		int i = Integer.parseInt(input.nextLine());
		
		if (i / 2 == 0 ) {
			System.out.println("foo");
		} else if ( i / 3 == 0 ) {
			System.out.println("bar");
		} else if ( i / 2 == 0 && i / 3 == 0 ) {
			System.out.println("foobar");
		} else {
			System.out.println("Error");
		}
		
		input.close();
	}

}
