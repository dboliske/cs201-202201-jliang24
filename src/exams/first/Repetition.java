package exams.first;

import java.util.Scanner;

public class Repetition {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner input = new Scanner(System.in);
		System.out.println("enter in triangle size:");
	
		int size = input.nextInt();
		
		// set the height and width
		for (int i=0; i<size.length; i++) { 
			for (int j=0; j<size.length; j++) {
				System.out.print("+");
			}
			System.out.println();
		}
		
		
		input.close();
	}

}
