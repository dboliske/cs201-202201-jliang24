package labs.lab3;

import java.util.Scanner;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class ExerciseTwo {
	public static void main(String[] args) throws IOException {

//Write a program that will continue to prompt the user for numbers
//storing them in an array, until they enter "Done" to finish
//then prompts the user for a file name so that these values can be saved to that file.
//For example, if the user enters "output.txt", then the program should write the numbers that have been read to "output.txt".
		
		
		Scanner input = new Scanner(System.in); // create scanner for user input
		
		double [] number = new double[5];
//		for (int i=0; i<values.length; i++) {
//			System.out.println("(" + (i+1) + ") number: ");
//			values[i] = Double.parseDouble(input.nextLine());
//		}
		String userInput;
		int count = 0;
		
		do {
		 userInput = input.nextLine();
		 if (userInput != "Done") {
			 if (count == number.length ) {
				 double [] temp = new double[number.length+5];
				 for ( int i=0; i<=number.length; i++) {
					 temp[i] = number[i];
				 }
				 number = temp;
				 temp = null;
			 }
			 number[count] = Double.parseDouble(userInput);
			 count++;
		 }
		 
		} while (userInput !=  "Done");
		double temp[] = new double [count];
		for (int i=0; i<=temp.length; i++) {
			temp[i]= number[i];
		}
		number=temp;
		temp=null;

		
		input.close();
		
		
	FileWriter output  =  new FileWriter ("src/output.txt");

	output.write("The user input\n");
	output.close();
	}

}
