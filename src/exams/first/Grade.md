# Midterm Exam

## Total

67/100

## Break Down

1. Data Types:                  15/20
    - Compiles:                 4/5
    - Input:                    5/5
    - Data Types:               3/5
    - Results:                  3/5
2. Selection:                   15/20
    - Compiles:                 5/5
    - Selection Structure:      5/10
    - Results:                  5/5
3. Repetition:                  16/20
    - Compiles:                 4/5
    - Repetition Structure:     5/5
    - Returns:                  5/5
    - Formatting:               2/5
4. Arrays:                      9/20
    - Compiles:                 4/5
    - Array:                    0/5
    - Exit:                     5/5
    - Results:                  0/5
5. Objects:                     12/20
    - Variables:                5/5
    - Constructors:             2/5
    - Accessors and Mutators:   3/5
    - toString and equals:      2/5

## Comments

1. Has compiler errors and doesn't correctly convert an integer to a character.
2. Does not correctly check for remainders using the modulus operator (`%`).
3. Prints a square, not a triangle and has compiler errors.
4. Compiler errors and does not read in 5 words or print the ones that occur more than once.
5. Neither constructor correctly assigns values to the instance variables, the mutator does not correctly validate `age` and there is no attempt at an `equals` method.
