package labs.lab1;

import java.util.Scanner;

public class boxProgram {

	public static void main(String[] args ) {
		// test program 
//		input                                     expected result
//		length: 8, width: 10, depth: 12,          960
//		length: 10, width: 9, depth: 20,          1080
//		length: 12, width: 8, depth: 14,          1344
////		
		Scanner input = new Scanner (System.in); // scanner
	
		// prompt user length, width, depth
		
		System.out.println("Enter length: ");
		System.out.println("Enter width: ");
		System.out.println("Enter depth: ");
		
		int length = Integer.parseInt(input.nextLine());
		int width = Integer.parseInt(input.nextLine());
		int depth = Integer.parseInt(input.nextLine());
		
		int result = Integer.parseInt(input.nextLine());
		System.out.println(length * width * depth); //calculations 
		
		System.out.println("You have this amount of wood " + result);
	
		input.close();
	}

}
