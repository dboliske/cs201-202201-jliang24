package labs.lab2;

import java.util.Scanner;

public class Menu {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner input = new Scanner (System.in);
		
		boolean done = false; // flag controller
		
		do {
			System.out.println("1. Print 'Hello'");
			System.out.println("2. Print 'Addition'");  
			System.out.println("3. Print 'Multiplication'");
			System.out.println("4. Exit");
			System.out.println("Choice: ");
			String choice = input.nextLine(); // get user input
			
			switch(choice) {
			case "1": System.out.println("Hello");
				break;
			case "2": 
//				prompt user two numbers and add the random two numbers and return it 
				System.out.println("a: ");
				System.out.println("b: ");
				double a = Double.parseDouble(input.nextLine());
				double b = Double.parseDouble(input.nextLine());
			
			// addition calculation 
				double result;
				result = a+b;
				System.out.println(result);
	
				break;
			case "3":
				// prompt the user 2 numbers then multiply it and return it 
				System.out.println("c: ");
				System.out.println("d: ");
				double c = Double.parseDouble(input.nextLine());
				double d = Double.parseDouble(input.nextLine());
				
			 // multiply two numbers and return it 
				double multiplicationResult;
				multiplicationResult = c*d;
				System.out.println(multiplicationResult);
				
				break;
			case "4":
				done = true;
				break;
			default: 
				System.out.println("That is not a valid choice");
			}
			
		} while (!done);
		
		input.close();
		
		System.out.println("Goodbye!");
	}

}
