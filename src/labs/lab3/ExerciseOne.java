package labs.lab3;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class ExerciseOne {

	public static void main(String[] args) throws IOException {
	
		File f =  new File ("src/lab.lab3/grades.csv"); // get the grades.csv file 
		Scanner input = new Scanner(System.in); // prompt user and get input 
		
		double values [] = new double [14]; // array of the grades file has a 14 column length
		for (int i=0; i<values.length; i++) {
			System.out.print("("+ (i+1) + ") number:"); 
			values[i] = Double.parseDouble(input.nextLine());
			
					
		}
		
		input.close();
		
		double total = 0;
		for (int i=0; i<values.length; i++) {
			total = total + values[i];
		}

		System.out.println("Average: " + (total / values.length)); // calculation for average
	}

}