package exam.second;

public abstract class Polygon {

	protected String name;
	
	public Polygon () {
		name = "abcdefghijklmnopqrstuyz";
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String toString() {
		return "name: " + name;
	}
	
	public double area() {
		int n = 0; // number of sides
		int a = 0; // side of lengths
		int ri = 0; // radius of the circle
		return (n * a * ri / 2);
	}
	
	public double perimeter() {
		int n1 = 0;
		int a2 = 0;
		return (n1*a2);
		
	}
}
