package labs.lab5;

import labs.lab4.GeoLocation;

public class CTAStaton extends GeoLocation {

	private String name;
	private String location;
	private boolean wheelchair;
	private boolean open;

	public CTAStaton() {
		super();
		this.name = "Roosevelt";
		this.location = "Chicago";
		
		}
	public CTAStaton (String name, double lat, double lng, String location, boolean wheelchair, boolean open) {
		super();
		this.name = "Roosevelt";
		this.location = "Chicago";
		this.lat = 0;
		this.lng = 0;
		this.wheelchair = null;
		this.open = null;;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public boolean isWheelchair() {
		return wheelchair;
	}
	public void setWheelchair(boolean wheelchair) {
		this.wheelchair = wheelchair;
	}
	public boolean isOpen() {
		return open;
	}
	public void setOpen(boolean open) {
		this.open = open;
	}
	 
	public boolean hasWheelChair() {
		return open;
	}
	
	public String toString () {
		return super.toString() + " " + super.getName() +  " " + super.getLocation() + "lat, lng";
	}
	
	public void equalsBoolean() {
		
	}
	
}
