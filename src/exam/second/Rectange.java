package exam.second;

public class Rectange extends Polygon {

	private double width;
	private double height;
	
	public Rectange() {
		this.width = 1;
		this.height = 1;
	}

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}
	
	public String toString() {
		return "Rectangle : " + width + height;
	}
	
	public double area() {
		return  width * height;
	}
	
	public double perimeter() {
		return  2* width + 2 * height;
	}
}
