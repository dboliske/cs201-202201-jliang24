package exam.second;

public class Sorting {
	

	public static String[] sort(String[] array) {
		for (int i=0; i<array.length; i++) {
			int min = i;
			for ( int j= i+1;j<array.length; j++) {
				if(array[j].compareTo(array[min]) < 0 ) {
					min = j;
					
				}
			}
		if ( min != i) {
			String temp = array[i];
			array[i] = array[min];
			array[min] = temp;
		}
		}
		return array;
	}


public static void main(String[] args) {
	String [] lang = {"speaker\", \"poem\", \"passenger\", \"tale\", \"reflection\", \"leader\", \"quality\", \"percentage\", \"height\", \"wealth\", \"resource\", \"lake\", \"importance\"}"};
	 lang = sort(lang);
	 for (String l : lang) {
		 System.out.print(l + " " );
	 }
}
}
