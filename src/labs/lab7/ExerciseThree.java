package labs.lab7;

public class ExerciseThree {

		public static String[] sort(String[] array) {
			for (int i=0; i<array.length; i++) {
				int min = i;
				for ( int j= i+1;j<array.length; j++) {
					if(array[j].compareTo(array[min]) < 0 ) {
						min = j;
						
					}
				}
			if ( min != i) {
				String temp = array[i];
				array[i] = array[min];
				array[min] = temp;
			}
			}
			return array;
		}


	public static void main(String[] args) {
		String [] lang = {"3.142", "2.718", "1.414", "1.732", "1.202", "1.618", "0.577", "1.304", "2.685", "1.282"};
		 lang = sort(lang);
		 for (String l : lang) {
			 System.out.print(l + " " );
		 }
		
	}

}
