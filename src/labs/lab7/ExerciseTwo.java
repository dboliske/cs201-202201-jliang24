package labs.lab7;

public class ExerciseTwo {
	
	public static String [] sort(String[] array) {
		for ( int j=1; j<array.length; j++) {
			int i = j;
			while (i>0 && array[i].compareTo(array[i-1]) < 0) {
				String temp= array[i];
				array[i] = array[i-1];
				array[i-1] = temp;
				i--;
			}
		}
		return array;
	}

	public static void main(String[] args) {
		String[] lang =  {"cat", "fat", "dog", "apple", "bat", "egg"};
		lang = sort(lang);
		
		for (String l: lang) {
			System.out.print(l + " ");
		}

	}

}
