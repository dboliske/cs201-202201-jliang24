# Final Project

## Total

129/150

## Break Down

Phase 1:                                                            39/50

- Description of the user interface                                 3/3
- Description of the programmer's tasks:
  - Describe how you will read the input                            3/3
  - Describe how you will process the data from the input file      3/4
  - Describe how you will store the data                            3/3
  - How will you add/delete/modify data?                            5/5
  - How will you search data?                                       5/5
- Classes: List of names and descriptions                           7/7
- UML Class Diagrams                                                0/10
- Testing Plan                                                      10/10

Phase 2:                                                            90/100

- Compiles and runs with no run-time errors                         8/10
- Documentation                                                     15/15
- Test plan                                                         8/10
- Inheritance relationship                                          5/5
- Association relationship                                          3/5
- Searching works                                                   5/5
- Uses a list                                                       5/5
- Project reads data from a file                                    5/5
- Project writes data to a file                                     5/5
- Project adds, deletes, and modifies data stored in list           5/5
- Project generates paths between any two stations                  8/10
- Project encapsulates data                                         5/5
- Project is well coded with good design                            8/10
- All classes are complete (getters, setters, toString, equals...)  5/5

## Comments you don't have an UML design for the project.

### Design Comments

### Code Comments
